<?php
/**
 * @var QiActiveForm $form
 * @var QiAuthItem $model
 */
?>

<div class="row">
	<div class="col-xs-12">
		<?php $form->qiSelectizeBlock($model, 'parentItemsArray', CHtml::listData(QiAuthItem::model()->findAll(), 'name', 'name'), [
			'create' => false, 'maxItems' => null, 'persist' => false
		]); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<?php $form->qiSelectizeBlock($model, 'childItemsArray', CHtml::listData(QiAuthItem::model()->findAll(), 'name', 'name'), [
			'create' => true, 'maxItems' => null, 'persist' => false
		]); ?>
	</div>
</div>
