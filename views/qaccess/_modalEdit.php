<?php
/**
 * @author chervand <chervand@gmail.com>
 * @var QiController $this
 * @var QiPageHelp $model
 * @var QiActiveForm $form
 */

$form = $this->beginWidget('QiActiveForm', [
	'id' => 'form-edit-auth-item',
	'errorMessageCssClass' => 'help-block',
	'enableAjaxValidation' => true,
	'clientOptions' => [
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => true,
		'inputContainer' => '.form-group',
		'errorCssClass' => 'error has-error',
		'successCssClass' => 'success has-success',
		'afterValidate' => 'js:function(form, data, hasErrors){
			if (hasErrors){
				$(form).find(".popover .loading").remove();
				$(form).find("input[name=create], input[name=save]").popover("toggle");
				return false;
			} else return true;
		}'
	]
]);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?= CHtml::value($model, 'typeLabel') ?>
		<small><?= CHtml::value($model, 'name', Yii::t('qi', 'New Auth Item')) ?></small>
	</h4>
</div>

<div class="modal-body">
	<?php $this->widget('QiTabs', [
		'tabs' => [
			[
				'active' => true,
				'tabLabel' => CHtml::tag('i', ['class' => 'icon-left fa fa-edit'], '') . Yii::t('qi', 'General'),
				'paneId' => 'auth-item-general',
				'paneContent' => $this->renderPartial('qi.qaccess.views.qaccess._modalEditGeneral', ['form' => $form, 'model' => $model], true)
			],
			[
				'tabLabel' => CHtml::tag('i', ['class' => 'icon-left fa fa-briefcase'], '') . Yii::t('qi', 'Bizrule & Data'),
				'paneId' => 'auth-item-bizrule',
				'paneContent' => $this->renderPartial('qi.qaccess.views.qaccess._modalEditBizrule', ['form' => $form, 'model' => $model], true)
			],
			[
				'tabLabel' => CHtml::tag('i', ['class' => 'icon-left fa fa-sitemap'], '') . Yii::t('qi', 'Dependencies'),
				'paneId' => 'auth-item-dependencies',
				'paneContent' => $this->renderPartial('qi.qaccess.views.qaccess._modalEditDependencies', ['form' => $form, 'model' => $model], true)
			]
		]
	]); ?>
	<div class="help-block"><?= Yii::t('qi', '* required fields') ?></div>
</div>

<div class="modal-footer">
	<?= CHtml::button(Yii::t('qadmin', 'Cancel'), [
		'name' => 'cancel',
		'class' => 'btn btn-default pull-left',
		'data-dismiss' => 'modal'
	]) ?>
	<?= CHtml::submitButton(Yii::t('qadmin', 'Save'), [
		'name' => 'save',
		'class' => 'btn btn-primary pull-right',
	]) ?>
	<?= CHtml::submitButton(Yii::t('qadmin', 'Delete'), [
		'name' => 'delete',
		'class' => 'btn btn-danger pull-right',
		'disabled' => $model->getIsNewRecord()
	]) ?>
</div>

<?php $this->endWidget('form-edit-auth-item'); ?>
