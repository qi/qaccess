<?php
/**
 * @var QiActiveForm $form
 * @var QiAuthItem $model
 */
?>

<div class="row">
	<div class="col-xs-12">
		<?php $form->qiTextAreaBlock($model, 'bizrule', ['style' => 'font-family:monospace;font-size:0.9em;']); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<?php $form->qiTextAreaBlock($model, 'data', ['style' => 'font-family:monospace;font-size:0.9em;']); ?>
	</div>
</div>
