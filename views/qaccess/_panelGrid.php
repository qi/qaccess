<?php
/**
 * @var QiController $this
 * @var QiAuthItem $model
 */

$this->beginWidget('QiPanel', [
	'id' => 'panel-grid-auth-item',
	'title' => CHtml::tag('i', ['class' => 'fa fa-sitemap icon-left'], '') . Yii::t('qaccess', 'Auth Items'),
	'controls' => [
		CHtml::dropDownList(get_class($model) . '_page_size', Yii::app()->user->getState(get_class($model) . '_page_size', 15), [
			15 => 15, 30 => 30, 50 => 50, 100 => 100
		], ['class' => 'form-control']),
		CHtml::htmlButton(CHtml::tag('i', ['class' => 'fa fa-plus icon-left'], '') . Yii::t('qadmin', 'Add'), [
			'class' => 'btn btn-default btn-flat',
			'data-toggle' => 'modal',
			'data-target' => '#modal-edit-auth-item',
			'data-source' => $this->createUrl('axModalEditAuthItem'),
		]),
	],
	'js' => '$("#panel-grid-auth-item").on("change", "#QiAuthItem_page_size", function () {
				$.fn.yiiGridView.update("api-logs", {data: {QiAuthItem_page_size: $(this).val()}});});'
]);

$this->beginWidget('QiPanelBody', ['id' => 'panel-grid-auth-item-body']);

$this->widget('QiGridView', [
	'id' => 'grid-auth-item',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => [
		[
			'name' => 'type',
			'filter' => $model->getTypeLabels(),
			'type' => 'raw',
			'value' => 'CHtml::tag("i", ["class" => "fa fa-tag text-info icon-left"], "") . $data->typeLabel',
			'headerHtmlOptions' => ['class' => 'col-xs-2'],
		],
		[
			'name' => 'name',
			'headerHtmlOptions' => ['class' => 'col-xs-5'],
		],
		[
			'name' => 'description',
			'headerHtmlOptions' => ['class' => 'col-xs-5'],
		],
		[
			'name' => 'bizrule',
			'type' => 'raw',
			'value' => '$data->bizrule ? CHtml::tag("i", ["class" => "fa fa-check text-success"], "") : ""',
			'filter' => false,
		],
		[
			'name' => 'data',
			'type' => 'raw',
			'value' => '$data->data ? CHtml::tag("i", ["class" => "fa fa-check text-success"], "") : ""',
			'filter' => false,
		],
		[
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag("i", ["class" => "fa fa-cog"], ""), "#", [
				"data-toggle" => "modal", "data-target" => "#modal-edit-auth-item",
				"data-source" => "' . $this->createUrl('axModalEditAuthItem?QiAuthItem[name]=$data->name') . '",
				"title" => "' . Yii::t('qadmin', 'Edit') . '"
			])'
		],
	],
]);

$this->widget('QiModal', ['id' => 'modal-edit-auth-item']);

$this->endWidget('panel-grid-auth-item-body');

$this->endWidget('panel-grid-auth-item');
