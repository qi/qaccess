<?php
/**
 * @var QiActiveForm $form
 * @var QiAuthItem $model
 */
?>

<div class="row">
	<div class="col-xs-12">
		<?php $form->qiTextFieldBlock($model, 'name', [], 'module/controller/action') ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-8">
		<?php $form->qiTextAreaBlock($model, 'description') ?>
	</div>
	<div class="col-xs-4">
		<?php $form->qiBeginBlock($model, 'type') ?>
		<?= $form->radioButtonList($model, 'type', QiAuthItem::getTypeLabels()) ?>
		<?php $form->qiEndBlock($model, 'type') ?>
	</div>
</div>
