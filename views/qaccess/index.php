<?php
/**
 * @var QiAuthItem $model
 */
?>

<div class="row">
	<div class="col-md-12">
		<?php $this->renderPartial('qi.qaccess.views.qaccess._panelGrid', ['model' => $model]); ?>
	</div>
</div>
