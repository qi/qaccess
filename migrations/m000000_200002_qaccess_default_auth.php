<?php

class m000000_200002_qaccess_default_auth extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default/*']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/default/index']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/default/login']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/default/logout']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/default/error']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/default/about']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/default/*', 'child' => 'dashboard/default/index']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/default/*', 'child' => 'dashboard/default/login']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/default/*', 'child' => 'dashboard/default/logout']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/default/*', 'child' => 'dashboard/default/error']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/default/*', 'child' => 'dashboard/default/about']);

		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/api/*']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/api/index']);
		$this->insert('qi_auth_item', ['type' => CAuthItem::TYPE_OPERATION, 'name' => 'dashboard/api/axModalDetailLog']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/api/*', 'child' => 'dashboard/api/index']);
		$this->insert('qi_auth_item_child', ['parent' => 'dashboard/api/*', 'child' => 'dashboard/api/axModalDetailLog']);

		$this->insert('qi_auth_item_child', ['parent' => 'guest', 'child' => 'dashboard/default/*']);
		$this->insert('qi_auth_item_child', ['parent' => 'support', 'child' => 'dashboard/default/*']);
		$this->insert('qi_auth_item_child', ['parent' => 'admin', 'child' => 'dashboard/api/*']);

	}

	public function safeDown()
	{

	}
}