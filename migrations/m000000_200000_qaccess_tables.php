<?php

class m000000_200000_qaccess_tables extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qi_auth_item', [
			'name' => 'varchar(64) not null',
			'type' => 'integer not null',
			'description' => 'text',
			'bizrule' => 'text',
			'data' => 'text',
			'primary key (`name`)',
		]);
		$this->createTable('qi_auth_item_child', [
			'parent' => 'varchar(64) not null',
			'child' => 'varchar(64) not null',
			'primary key (`parent`,`child`)',
			'foreign key (`parent`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
			'foreign key (`child`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
		]);
		$this->createTable('qi_auth_assignment', [
			'itemname' => 'varchar(64) not null',
			'userid' => 'varchar(64) not null',
			'bizrule' => 'text',
			'data' => 'text',
			'primary key (`itemname`,`userid`)',
			'foreign key (`itemname`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
		]);
	}

	public function safeDown()
	{
		$this->dropTable('qi_auth_assignment');
		$this->dropTable('qi_auth_item_child');
		$this->dropTable('qi_auth_item');
	}
}