<?php

class m000000_200001_qaccess_default_roles extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('qi_auth_item', [
			'name' => 'admin',
			'type' => CAuthItem::TYPE_ROLE,
			'bizrule' => 'return Yii::app()->user->isAdmin;',
		]);
		$this->insert('qi_auth_item', [
			'name' => 'moder',
			'type' => CAuthItem::TYPE_ROLE,
			'bizrule' => 'return Yii::app()->user->isModer;',
		]);
		$this->insert('qi_auth_item', [
			'name' => 'support',
			'type' => CAuthItem::TYPE_ROLE,
			'bizrule' => 'return Yii::app()->user->isSupport;',
		]);
		$this->insert('qi_auth_item', [
			'name' => 'user',
			'type' => CAuthItem::TYPE_ROLE,
			'bizrule' => 'return Yii::app()->user->isUser;',
		]);
		$this->insert('qi_auth_item', [
			'name' => 'guest',
			'type' => CAuthItem::TYPE_ROLE,
			'bizrule' => 'return Yii::app()->user->isGuest;',
		]);

		$this->insert('qi_auth_item_child', ['parent' => 'admin', 'child' => 'moder']);
		$this->insert('qi_auth_item_child', ['parent' => 'moder', 'child' => 'support']);
	}

	public function safeDown()
	{
		$this->delete('qi_auth_item', 'name=\'guest\'');
		$this->delete('qi_auth_item', 'name=\'user\'');
		$this->delete('qi_auth_item', 'name=\'support\'');
		$this->delete('qi_auth_item', 'name=\'moderator\'');
		$this->delete('qi_auth_item', 'name=\'admin\'');
	}
}