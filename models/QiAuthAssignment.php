<?php

/**
 * Class QiAuthAssignment
 * @property string $itemname
 * @property string $userid
 * @property string $bizrule
 * @property string $data
 */
class QiAuthAssignment extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'qi_auth_assignment';
	}

	public function rules()
	{
		return [];
	}

	public function relations()
	{
		return [];
	}

	public function attributeLabels()
	{
		return [];
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}
