<?php

/**
 * Class QiAuthItem
 * @property string $name
 * @property string $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 */
class QiAuthItem extends CActiveRecord
{
	private $_parentItemsArray;
	private $_childItemsArray;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'qi_auth_item';
	}

	public function rules()
	{
		return [
			['name, type', 'required'],
			['type', 'in', 'range' => array_keys(self::getTypeLabels())],
			['description, bizrule, data', 'default', 'value' => null],
			['description, bizrule, data, childItemsArray, parentItemsArray', 'safe'],
			['name, type, description, bizrule, data', 'safe', 'on' => 'search']
		];
	}

	public function relations()
	{
		return [
			'childItems' => [self::HAS_MANY, 'QiAuthItem', ['child' => 'name'], 'through' => 'childRelations'],
			'childRelations' => [self::HAS_MANY, 'QiAuthItemChild', 'parent'],
			'parentItems' => [self::HAS_MANY, 'QiAuthItem', ['parent' => 'name'], 'through' => 'parentRelations'],
			'parentRelations' => [self::HAS_MANY, 'QiAuthItemChild', 'child'],
		];
	}

	public function attributeLabels()
	{
		return [];
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.type', $this->type);
		$criteria->compare('t.description', $this->description, true);
		$criteria->compare('t.bizrule', $this->bizrule, true);
		$criteria->compare('t.data', $this->data, true);
		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
			'sort' => [
				'defaultOrder' => 't.type DESC, t.name ASC'
			]
		]);
	}

	public static function getTypeLabels()
	{
		return [
			CAuthItem::TYPE_OPERATION => Yii::t('qaccess', 'Operation'),
			CAuthItem::TYPE_TASK => Yii::t('qaccess', 'Task'),
			CAuthItem::TYPE_ROLE => Yii::t('qaccess', 'Role'),
		];
	}

	public function getTypeLabel()
	{
		return self::getTypeLabels()[$this->type];
	}

	protected function beforeValidate()
	{
		if (parent::beforeValidate()) {
			// TODO:save parent and child items
			return true;
		}
		return false;
	}

	public function getChildItemsArray()
	{
		$this->_childItemsArray = CHtml::listData($this->childItems, 'name', 'name');
		return $this->_childItemsArray ? $this->_childItemsArray : [];
	}

	public function setChildItemsArray($items)
	{
		$this->_childItemsArray = $items;
	}

	public function getParentItemsArray()
	{
		$this->_parentItemsArray = CHtml::listData($this->parentItems, 'name', 'name');
		return $this->_parentItemsArray ? $this->_parentItemsArray : [];
	}

	public function setParentItemsArray($items)
	{
		$this->_parentItemsArray = $items;
	}
}
