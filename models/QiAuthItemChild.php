<?php

/**
 * Class QiAuthItemChild
 * @property string $parent
 * @property string $child
 */
class QiAuthItemChild extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'qi_auth_item_child';
	}

	public function rules()
	{
		return [];
	}

	public function relations()
	{
		return [];
	}

	public function attributeLabels()
	{
		return [];
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}
