<?php

class QaccessController extends QiController
{
	public function actions()
	{
		return [
			'axModalEditAuthItem' => [
				'class' => 'qi.qadmin.actions.axEditModal',
				'modalView' => 'qi.qaccess.views.qaccess._modalEdit',
				'formId' => 'form-edit-auth-item',
				'models' => [
					'model' => [
						'class' => 'QiAuthItem',
						'pk' => 'name'
					]
				],
			],
		];
	}

	public function actionIndex()
	{
		$model = new QiAuthItem('search');
		$model->unsetAttributes();
		if (isset($_GET['QiAuthItem']))
			$model->attributes = $_GET['QiAuthItem'];

		$this->render('qi.qaccess.views.qaccess.index', [
			'model' => $model
		]);
	}
}